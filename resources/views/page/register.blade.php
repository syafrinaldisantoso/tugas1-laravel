@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
     
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <Label>First Name</Label> <br>
        <input type="text" name="first_name"> <br> <br>

        <label>Last Name</label> <br>
        <input type="text" name="last_name"> <br> <br>

        <label>Gender</label> <br>
        <input type="radio" name="gn"> Male <br>
        <input type="radio" name="gn"> Female <br> <br>

        <label>Nationality</label> <br>
        <select name="Nationality" id="">
            <option value="1">Indonesia</option>
            <option value="2">Palestine</option>
        </select> <br> <br>

        <label>Language Spoken</label> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>

        <label>Bio</label> <br>
        <textarea name="Bio"  cols="30" rows="10"></textarea> <br> <br>

    <input type="submit" value="Sign Up"> 
    </form>
   
@endsection