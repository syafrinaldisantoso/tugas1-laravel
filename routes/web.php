<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-table', 'IndexController@table');

// CRUD Cast
// create
Route::get('/cast/create', 'CastController@create'); //mengarah ke form tambah data
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast

//Read
Route::get('/cast', 'CastController@index'); //ambil data ke database kemudian tampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show'); //route detail kategori

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route untuk mengarah ke form edit 
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //route delete data berdasarkan id